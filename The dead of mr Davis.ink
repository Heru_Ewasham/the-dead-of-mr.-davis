/*
* Made by Yngve Hestem
*/

//Adding some global tags:
#author: Yngve Hestem
#theme: dark

//Setting some constants:
CONST VICTIM = "Tom Davis"          // Victims full name
CONST VICTIM_FORMAL = "Mr. Davis"   // Victims name in a formal tone

// Rooms:
CONST LIVINGROOM = 0
CONST CRIMESCENE = 1
CONST OUTDOOR = 2
CONST BLUE_HOUSE = 3
CONST GENERAL_HOUSE = 4
CONST KLUPP_HOUSE = 5
CONST EVIDENCE_FOLDER = 6

// Setting some global variables:
VAR askedAboutWound = false
VAR knowAllSuspects = false         // True if all suspects is known
VAR numberOfEvidences = 0
VAR foundEvidenceWound = false      // True if talked about wound with police doctor
VAR foundEvidenceGender = false     // True if talked about gender with -- || --
VAR foundEvidenceCancer = false     // When he finds out of cancer
VAR foundEvidenceLadyClothes = false
VAR foundEvidenceKlaraAngry = false
VAR foundEvidenceBlueWasAtHome = false
VAR foundEvidenceGeneralWasAtPub = false
VAR foundEvidenceArguing = false
VAR foundEvidenceArguingAnswerDoctor = false
VAR foundEvidenceArguingAnswerKlara = false
VAR foundEvidenceTimeOfReporting = false
VAR foundEvidenceVictimIll = false
VAR foundEvidenceKlaraMan = false
VAR foundEvidenceRelationshipKlupp = false
VAR foundEvidenceRelationshipJustin = false
VAR foundEvidenceRelationshipGeneral = false
VAR foundEvidenceLastNightKlupp = false
VAR foundEvidenceLastNightKlara = false
VAR foundEvidenceKnowVictimKlara = false
VAR foundEvidenceKnowVictimGeneral = false



// Setting places where NPC are:
VAR playerArea = OUTDOOR
VAR doctorDerek = LIVINGROOM
VAR madameKlupp = KLUPP_HOUSE
VAR klaraBlue = BLUE_HOUSE
VAR justinBlue = BLUE_HOUSE
VAR generalSistner = GENERAL_HOUSE

-> Beginning    // Start the first scene in the game

// Be ready for a new scene or clear out screen
=== function initScene(scene) ===
{clear()}
~ playerArea = scene

=== function clear ==
# CLEAR


== Beginning ==
It was a nice day. The sun was shining and Dr. Derek walked up the walkway from his car to the house. The house was owned by a guy named {VICTIM}. He was a bussiness man, but lately he had become very ill.

Dr. Derek opened the door and announced that he was there by yelling "{VICTIM_FORMAL}, it's me! Are you feeling better today?". No answer. He walked into the bedroom...

...There, {VICTIM} was laying, dead.

* [Continue]
-> StartInvestigation


== StartInvestigation
{initScene(OUTDOOR)}
# background: outdoor   // Set a tag with info about which background to show

Police Inspector Rick Green was an experienced Investigator. He had worked 11 years, and wanted to be there for many more years to come.

"Sir, let me give you an update." a police officer said.

* "Thanks"
* "Please, I need an update"
- "OK, I will be short. The doctor found also the victim dead at the bedroom at 12.05 today. The doctor thought first it was a normal death, but when he found the wound he understood it was a murder."
{foundEvidenceTimeOfReporting == false:
    ~ foundEvidenceTimeOfReporting = true
    ~ numberOfEvidences++
}
    -- (SomethingMore) "Is it some more info you need before we begin?"
    ** "Is the doctor here?"
        "Yes, he is inside, in the living room."
    ** "Why did he first thoght it was a normal death?"
        "The victim was very ill sir. I would suggest you talk to the doctor for more info."
        ~ foundEvidenceVictimIll = true
        ~ numberOfEvidences++
    ** "What do you mean by 'when' he found the wound?"
        "The wound was, well, on a special place sir. You will understand better when you see it yourself."
        ~askedAboutWound = true
    -- -> SomethingMore
+ [Go inside]
    "Let's go inside, shall we?" Rick announced.
    -> crimeScene
    


== crimeScene ==
{initScene(CRIMESCENE)}
# background: crimescene   // Set a tag with info about which background to show

The crime scene was a mess. It looked like {VICTIM_FORMAL} had not cleaned there in many years, and when all the people in blue overrals had added evidence signs to the mess, everything had been much vorse.

One of the men in a blue overrall walked towards Rick and said <>

- (SomethingMore) "What do you want to do?"
# background: crimescene    // Set a tag with info about which background to show
<- Choices(-> crimeScene)
** "Let me see the body."
    "As you can see, the wound is down in the region around the penis, a knife was used to make it. He didn't survive because of the bad wound. A painfull dead."
    {askedAboutWound: "I understand what you mean" Rick said to the police officer who had coming with him up. | "Mmm" rick replayed.}
    ~ foundEvidenceWound = true
    ~ numberOfEvidences++
** "Can you tell me something about the murder?"
    "The wound is so deep that it is most likely a man who is behind, but if it is a very strong woman, she could be a potencial killer. I can also tell you that he died last night, between 21.00 and 24.00."
    ~ foundEvidenceGender = true
    ~ numberOfEvidences++
** See around the room
    In the room it is a completley mess. With a lot of evidence signs and things laying all around."
-- -> SomethingMore
    

== LivingRoom ==
{initScene(LIVINGROOM)}
# background: livingroom   // Set a tag with info about which background to show
What will you do?
<- Choices(-> LivingRoom)
->DONE

== KluppHouse ==
{initScene(KLUPP_HOUSE)}
# background: kluppHouse   // Set a tag with info about which background to show
Madame Klupp's living room was full of light. In the fireplace at the corner it was burning already, even though it still was hot outside. Rick had been invited on some tea or coffee, but had politley said no.

What will you do?
<- Choices(-> KluppHouse)
->DONE

== BlueHouse ==
{initScene(BLUE_HOUSE)}
# background: blueHouse   // Set a tag with info about which background to show
Rick met the Blue's inside their living room. He was shocked over to see how the Blue's looked like. Mr. Blue was thin, and looked quite normal, but Klara Blue looked almost as a man when she stood there.
{foundEvidenceKlaraMan == false:
    ~ foundEvidenceKlaraMan = true
    ~ numberOfEvidences++
}


What will you do?
<- Choices(-> BlueHouse)
->DONE

== GeneralHouse ==
{initScene(GENERAL_HOUSE)}
# background: generalHouse   // Set a tag with info about which background to show
The General's house looked very clean. He had most likely done as they does in the army, also keep the rooms always clean.
What will you do?
<- Choices(-> GeneralHouse)
->DONE



// Choiches that is most often availible.
=== Choices(-> go_back_to) ===
// Give the possibillity to talk to NPC if in the same room as player:
{ doctorDerek == playerArea:
    <- DoctorDerekConversation(go_back_to)
}
{ madameKlupp == playerArea:
    <- MadameKluppConversation(go_back_to)
}
{ klaraBlue == playerArea:
    <- KlaraBlueConversation(go_back_to)
}
{ justinBlue == playerArea:
    <- JustinBlueConversation(go_back_to)
}
{ generalSistner == playerArea:
    <- GeneralSistnerConversation(go_back_to)
}

//If have heard about all players, give the possibillity to say who it is:
// Crime scene is known from start, so no need to wait.
{playerArea != CRIMESCENE:
    + [Go to crime scene]
        -> crimeScene
}
{playerArea != LIVINGROOM:
    + [Go to {VICTIM_FORMAL}'s living room]
        -> LivingRoom
}

// If suspects is known, make it possible to go there.
{ knowAllSuspects:
// Go to a location if not already there:
    {playerArea != KLUPP_HOUSE:
        + [Go to Madame Klupps home]
            -> KluppHouse
    }
    {playerArea != GENERAL_HOUSE:
        + [Go to General Sistners home]
            -> GeneralHouse
    }
    {playerArea != BLUE_HOUSE:
        + [Go to Justin and Klara Blue's home]
            -> BlueHouse
    }
    
    
    // Give someone the blame and finish the game
    + "I know who killed Mr. Davis!"
    -> End(go_back_to)
}
+ [See on evidences]
    -> Evidences(go_back_to)



// Conversations:
    
=== DoctorDerekConversation(-> go_back_to) ===
+ [Talk to dr. Derek]
{clear()}
    - (SomethingMore) "Hello, just ask me anything!", Dr. Derek says.
    {foundEvidenceArguing:
        ** "I heard that you and {VICTIM_FORMAL} was arguing some time ago?"
            "Yes, it was nothing, just some arguing about some medication."
            ~ foundEvidenceArguingAnswerDoctor = true
            ~ numberOfEvidences++
            -> SomethingMore
    }
    ** "I heard you first thought it was a normal death?"
        "Yes, he was very ill you see. He had cancer and would most likely not survive more than some months to go anyway."
        ~ foundEvidenceCancer = true
        ~ numberOfEvidences++
    ** "Do you know who could have killed {VICTIM_FORMAL}?"
        "I am not sure, but General Sistner was a good friend of {VICTIM_FORMAL}, so he did most likely not kill him, but he could maybe give you a good explanation on how {VICTIM_FORMAL} was. We have also Justin and Klara Blue, they live not long from here, but I am not sure what the reason for them to kill him. And of course, we have the neighbour, Madame Klupp, which maybe could give you some information."
        {knowAllSuspects == false:
            ~knowAllSuspects = true
            ~numberOfEvidences++
        }
    ** "Do you have any idea who could give any light on what has happened"
     "I am not sure, but General Sistner was a good friend of {VICTIM_FORMAL}, he could maybe give you a good explanation on how he was. We have also Justin and Klara Blue, they live not long from here. And of course, we have the neighbour, Madame Klupp."
        {knowAllSuspects == false:
            ~knowAllSuspects = true
            ~numberOfEvidences++
        }
      -- -> SomethingMore
+ "Thanks, I need maybe to talk more to you later."
-> go_back_to


=== MadameKluppConversation(-> go_back_to) ===
+ [Talk to Madame Klupp]
{clear()}
    - (SomethingMore) "Hello, are you going to ask me something?", Madame Klupp says.
    ** "Have you seen anything?"
        "No, I don't think so. Or, wait, I think he got a visitor last night. But I could not see him, but it looked as a male who like to go in lady-clothes, since the person looked heavy out, but was in lady-clothes as if he had liked it."
        ~ foundEvidenceLadyClothes = true
        ~ numberOfEvidences++
    ** "As I understand, you and {VICTIM_FORMAL} are neighbours?"
        "Yes, we are."
    ** "Had you two a good relationship?"
        "Of course, we was friends, but not anything else if you thought something like that also."
        ~ foundEvidenceRelationshipKlupp = true
        ~ numberOfEvidences++
    ** "Where was you last night?"
        "I was here all night."
        ~ foundEvidenceLastNightKlupp = true
        ~ numberOfEvidences++
      -- -> SomethingMore
+ "Thanks, I need maybe to talk more to you later."
-> go_back_to

=== KlaraBlueConversation(-> go_back_to) ===
+ [Talk to Klara Blue]
{clear()}
    - (SomethingMore) "What will you ask me?", Klara asks.
    {foundEvidenceArguing:
        ** "I heard that you and {VICTIM_FORMAL} was arguing some time ago?"
            "Arguing, what arguing? I have not argued with {VICTIM_FORMAL}." Klara says shocked.
            ~ foundEvidenceArguingAnswerKlara = true
            ~ numberOfEvidences++
            -> SomethingMore
    }
    ** "Was your relationship good?"
        "Of course it was!" yelled Klara out. "Why do you think of saying shouch things."
        ~ foundEvidenceKlaraAngry = true
        ~ numberOfEvidences++
    ** "How do you know {VICTIM_FORMAL}?"
        "It's my housband who knows him. They work together."
        ~ foundEvidenceKnowVictimKlara = true
        ~ numberOfEvidences++
    ** "Where was you last night?"
        "Me and my housband was here.
        ~ foundEvidenceLastNightKlara = true
        ~ numberOfEvidences++
      -- -> SomethingMore
+ "Thanks, I need maybe to talk more to you later."
-> go_back_to

=== JustinBlueConversation(-> go_back_to) ===
+ [Talk to Justin Blue]
{clear()}
    - (SomethingMore) "What will you ask me?", Justin asks.
    ** "Was your relationship good?"
        ~ foundEvidenceRelationshipJustin = true
        ~ numberOfEvidences++
        "Yes, of course." Justin says politely.
    ** "How do you know {VICTIM_FORMAL}?"
        "We work together."
    ** "Where was you last night?"
        "Me and my wife was home in bed, I think. I slept so I can't tell if my wife was here all the time I am afraid
        ~ foundEvidenceBlueWasAtHome = true
        ~ numberOfEvidences++
      -- -> SomethingMore
+ "Thanks, I need maybe to talk more to you later."
-> go_back_to

=== GeneralSistnerConversation(->go_back_to) ===
+ [Talk to General Sistner]
{clear()}
    - (SomethingMore) "What will you ask me?", the General asks.
    ** "Was your relationship good?"
        "Yes, I would say so." the General says politely.
        ~ foundEvidenceRelationshipGeneral = true
        ~ numberOfEvidences++
    ** "How do you know {VICTIM_FORMAL}?"
        "We have been friends for so long that I can't remember any longer how we met, but I think it was in the army."
        ~ foundEvidenceKnowVictimGeneral = true
        ~ numberOfEvidences++
    ** "Do you know anything else that can be interesting?"
        "Well, last time we met, it was a party with all of his friends. That included the Blue's and Dr. Derek. Then both Dr. Derek and Klara Blue was arguing with {VICTIM_FORMAL}. But I don't know what they were arguing about."
        ~ foundEvidenceArguing = true
        ~ numberOfEvidences++
    ** "Where was you last night?"
        "I was at the pub, just call the pub-owner, he knows I was there."
            *** [Call the pub owner]
                "Hello" said it in the speaker. "It's the pub-owner speaking."
                **** "This is the police, was General Sistner at your pub last night?"
                    "Yes, I can confirm that he was there last night, as every other night."
                    ~ foundEvidenceGeneralWasAtPub = true
                    ~ numberOfEvidences++
                    ->SomethingMore
      -- -> SomethingMore
+ "Thanks, I need maybe to talk more to you later."
-> go_back_to

=== Evidences(-> go_back_to) ===
{initScene(EVIDENCE_FOLDER)}
# background: evidenceFolder   // Set a tag with info about which background to show
Number of evidences found: {numberOfEvidences}
{ numberOfEvidences > 0:
    {foundEvidenceTimeOfReporting: The victim was found at 12.05.}
    {foundEvidenceVictimIll: The victim was ill. Dr. Derek knows more about the illness.}
    {foundEvidenceWound: The murder murdered {VICTIM_FORMAL} by using a knife to wound him badly. A painfull death.}
    {foundEvidenceGender: The murderer is most likely a man, but it could also be a very strong woman.}
    {knowAllSuspects: Suspects: Madame Klupp, Dr. Derek, Klara and Justin Blue and General Sistner}
    {foundEvidenceCancer: {VICTIM_FORMAL} had cancer.}
    {foundEvidenceLadyClothes: Madame Klupp says she saw something that looked as a man in women chlotes going visiting {VICTIM_FORMAL} last night.}
    {foundEvidenceKlaraAngry: Klara got angry when asked about if they had a good relationship or not.}
    {foundEvidenceBlueWasAtHome: Justin says he was sleeping, and think his wife was too, but could not be sure.}
    {foundEvidenceGeneralWasAtPub: The General was at the local pub.}
    {foundEvidenceArguing: Dr. Derek and Klara Blue both was arguing with {VICTIM_FORMAL} last time the General saw them.}
    {foundEvidenceArguingAnswerDoctor: Dr. Derek was arguing with {VICTIM_FORMAL} about some medication, but it was not a very long argument according to Dr. Derek.}
    {foundEvidenceArguingAnswerKlara: Klara Blue did not argue with {VICTIM_FORMAL} according to herself. She sounded very shocked about been asked about that.}
    {foundEvidenceKlaraMan: While Justin Blue looks normal, Klara is heavily build.}
    {foundEvidenceRelationshipKlupp: Madame Klupp and {VICTIM} was friends, but nothing more than that.}
    {foundEvidenceRelationshipJustin: Justin says they had a good relationship.}
    {foundEvidenceRelationshipGeneral: The General says they had a good relationship.}
    {foundEvidenceKnowVictimKlara: Klara knows the victim via her housband, Justin, since they work togheter.}
    {foundEvidenceKnowVictimGeneral: The General don't remember when he met the victim, but think it was in the army.}
    {foundEvidenceLastNightKlupp: Madame Klupp says she was at home last night.}
    {foundEvidenceLastNightKlara: Klara Blue says she was at home last night.}
  - else:                               // If people can see this before any
                                        // evidences is collected    
    You have not found any evidences yet. Talk to people to get more evidences.
}
+ [Close evidence-folder]
    {clear()}
    -> go_back_to


// Tell who killed Mr. Davis
== End(-> go_back_to) ==
~ temp answer = "unknown"       // Default value when setting variable
~ temp correctAnswer = false
Who killed {VICTIM_FORMAL}?
* [Dr. Derek]
    ~ answer = "Dr. Derek"
* [Madame Klupp]
    ~ answer = "Madame Klupp"
* [Klara Blue]
    ~ answer = "Klara Blue"
    ~ correctAnswer = true
* [Justin Blue]
    ~ answer = "Justin Blue"
* [General Sistner]
    ~ answer = "General Sistner"
* [Suicide]
* "Wait, I need to think a little longer"
    -> go_back_to
    ~ answer = "{VICTIM_FORMAL}"
- {answer} is {correctAnswer: the person who did it. She did not like him, and since she was heavy she could easly be taken as a man. | sadly not the person who did it. Better luck next time.}

Credits:
Music from www.zapsplat.com and images from www.pexels.com. This game is made with ink (www.inklestudios.com/ink/).
    -> END